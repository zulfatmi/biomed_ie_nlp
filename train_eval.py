from argparse import ArgumentParser
from data_loader import DataLoader
from vocab import Vocab
import time
import numpy as np


def train_epoch(criterion, lr, optimizer, scheduler):
    model.train() # Turn on the train mode
    total_loss = 0.
    start_time = time.time()
    src_mask = model.generate_square_subsequent_mask(bptt).to(device)
    for batch, i in enumerate(range(0, train_data.size(0) - 1, bptt)):
        data, targets = get_batch(train_data, i)
        optimizer.zero_grad()
        if data.size(0) != bptt:
            src_mask = model.generate_square_subsequent_mask(data.size(0)).to(device)
        output = model(data, src_mask)
        loss = criterion(output.view(-1, ntokens), targets)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
        optimizer.step()

        total_loss += loss.item()
        log_interval = 200
        if batch % log_interval == 0 and batch > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            print('| epoch {:3d} | {:5d}/{:5d} batches | '
                  'lr {:02.2f} | ms/batch {:5.2f} | '
                  'loss {:5.2f} | ppl {:8.2f}'.format(
                    epoch, batch, len(train_data) // bptt, scheduler.get_lr()[0],
                    elapsed * 1000 / log_interval,
                    cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()


def idxs2str(vocab, idxs):
    strs = []
    for idx in idxs:
        strs.append(' '.join([vocab[t] for t in idx]))
    return strs


def evaluate(vocab, model, data_source, concepts):
    model.eval() # Turn on the evaluation mode
    pred = []
    src_mask = model.generate_square_subsequent_mask(bptt).to(device)
    with torch.no_grad():
        for i in range(0, data_source.size(0) - 1, bptt):
            data, targets = get_batch(data_source, i)
            if data.size(0) != bptt:
                src_mask = model.generate_square_subsequent_mask(data.size(0)).to(device)
            output = eval_model(data, src_mask)
            output_flat = output.view(-1, ntokens)
            output_str = idxs2str(model.vocab, output_flat)
            output_concepts = vocab.get_concept(output_str)
            pred += output_concepts
    return np.mean([p==t for p,t in zip(pred, concepts)])


bptt = 35
def get_batch(source, i):
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].reshape(-1)
    return data, target


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--train_fpath')
    parser.add_argument('--test_fpath')
    parser.add_argument('--vocab')
    parser.add_argument('--emsize', type=int)
    parser.add_argument('--nhid', type=int)
    parser.add_argument('--nlayers', type=int)
    parser.add_argument('--nhead', type=int)
    parser.add_argument('--dropout', type=float)
    parser.add_argument('--epochs', type=int)
    parser.add_argument('--save_to')
    args = parser.parse_args()

    data_loader = DataLoader(args.train_fpath)
    ntokens = len(data_loader.vocab.stoi)
    device = 1
    model = TransformerModel(ntokens, args.emsize, args.nhead, args.nhid, args.nlayers, args.dropout).to(device)

    criterion = nn.CrossEntropyLoss()
    lr = 5.0 # learning rate
    optimizer = torch.optim.SGD(model.parameters(), lr=lr)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)

    for epoch in range(1, args.epochs + 1):
        epoch_start_time = time.time()
        train_epoch(criterion, lr, optimizer, scheduler)
        print('| end of epoch {:3d} | time: {:5.2f}s | '.format(epoch, (time.time() - epoch_start_time))
        scheduler.step()

    torch.save(model.state_dict(), args.save_to)
    vocab = Vocab(args.vocab)
    concepts = iter(io.open(args.concepts, encoding="utf8"))
    test_data_loader = DataLoader(args.test_fpath)
    F = evaluate(vocab, model, data_source, concepts)
    print("Evaluation metric on test set is equal to:", F)
