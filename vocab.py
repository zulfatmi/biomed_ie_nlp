import pandas as pd
import numpy as np


def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]


class Vocab:
    def __init__(self, vpath):
        self.vocab = pd.read_csv(vpath, names=['concept_id', 'concept_name'], encoding='utf-8', sep='\t')
        
    def get_concept(self, output_strs):
        concepts = [] 
        for output_str in output_strs:
            distances = [levenshteinDistance(output_str, conceot_name) for conceot_name in self.vocab.conceot_name.tolist()]
            idx = np.argmin(distances)
            concept = self.vocab.concept_name.iloc[idx]
            concepts.append(concept)
        return concepts
